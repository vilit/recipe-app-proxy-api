# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

This is a proxy application for our API gateway and its friggin awesome. For easier development I have put in place whole CI/CD pipeline using GitLab and AWS ECR. 

### Environment Variables


* 'LISTEN_PORT' - Port to listen on (default: 8000)
* 'APP_HOST' - Hostname of the app to forward requests to (default: app)
* 'APP_PORT' - Port of the app to forward requests to (default: 9000)